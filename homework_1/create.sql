--SET client_encoding TO 'UTF8';

CREATE DATABASE avito
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

\c avito

CREATE TABLE regions
(
    region_id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(30)
);


CREATE TABLE users
(
    user_id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(30),
    region_id INT references regions (region_id)
);


CREATE TABLE bulletins
(
    bulletin_id SERIAL,
    content CHARACTER VARYING(30),
    user_id INT references users (user_id)
);

INSERT INTO regions VALUES (1,'Reg 1');
INSERT INTO regions VALUES (2,'Reg 2');
INSERT INTO regions VALUES (3,'Reg 77');
INSERT INTO regions VALUES (4,'Reg 68');
INSERT INTO regions VALUES (5,'Reg 3');

INSERT INTO users VALUES (1,'U 1', 1);
INSERT INTO users VALUES (2,'U 2', 2);
INSERT INTO users VALUES (3,'U 3', 3);
INSERT INTO users VALUES (4,'U 4', 4);
INSERT INTO users VALUES (5,'U 5', 5);

INSERT INTO bulletins VALUES (1,'BB 1', 1);
INSERT INTO bulletins VALUES (2,'BB 2', 2);
INSERT INTO bulletins VALUES (3,'BB 3', 3);
INSERT INTO bulletins VALUES (4,'BB 4', 4);
INSERT INTO bulletins VALUES (5,'BB 5', 5);