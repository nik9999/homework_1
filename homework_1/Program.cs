﻿using Microsoft.Extensions.CommandLineUtils;
using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework_1
{
    internal class Program
    {
        static  string sqlCS = @"Host=serv22.krata1.local;Port=5432;User Id=postgres;Password=masterkey;Database=avito;";
        static void Main(string[] args)
        {
            var app = new CommandLineApplication()
            {
                Name = "homework_1.exe",
                FullName = "homework_1",
                Description = "Home Work 1"
            };

            app.HelpOption("-?|-h|--help");
            app.VersionOption("--version", "1.0.0");

            var showOption = app.Option("-s|--show", "Просмотр БД Авито", CommandOptionType.NoValue);

            //homework_1.exe add [table] --data < DATA >
            app.Command("add", command =>
            {
                command.Description = "Добавление в таблицу";
                command.HelpOption("-?|-h|--help"); 

                var tableArg = command.Argument("table", "Имя таблицы bulletins, regions или users ");
                var dataOption = command.Option("-d|--data <DATA>", "Данные для строки", CommandOptionType.MultipleValue);

                command.OnExecute(() =>
                {
                    Console.WriteLine("Добавление записи...");
                    var table = tableArg.Value;
                    var data = dataOption.Value().ToString().Split(',');

                    using (var conn = new NpgsqlConnection(sqlCS))
                    {
                        try
                        {
                            conn.Open();
                            using (var com = conn.CreateCommand())
                            {
                                com.CommandText = $"INSERT INTO {table} VALUES ({data[0]},{data[1]}, {data[2]});";
                                com.ExecuteNonQuery();

                                Console.WriteLine("Запись добавлена!");
                            }
                        }

                        catch (Exception ex)  { Console.WriteLine(ex.ToString()); }
                        finally { conn.Close(); }
                    }

                    Console.ReadLine();
                    return 0;
                });
            });

            app.OnExecute(() =>
            {

                if (showOption.HasValue())
                {
                    Console.WriteLine("Просмотр БД Авито...");

                    using (var conn = new NpgsqlConnection(sqlCS))
                    {
                        try
                        {
                            conn.Open();

                            using (var command = conn.CreateCommand())
                            {
                                command.CommandText = @"SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_type='BASE TABLE';";

                                using (var reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {

                                        try
                                        {
                                            Console.WriteLine("Table [{0}] ", reader.GetString(0));

                                            showTable(reader.GetString(0));
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine(ex.Message);
                                        }
                                    }

                                    reader.Close();
                                }
                            }
                        }

                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
                else
                {
                    app.ShowHint();
                }

                Console.ReadLine();
                return 0;
            });
            try
            {
                app.Execute(args);
            }
            catch (CommandParsingException ex)
            {
                Console.WriteLine(ex.Message);
                app.ShowHelp();
                Console.ReadLine();
            }
        }

        static void showTable(string tableName)
        {
            using (var conn = new NpgsqlConnection(sqlCS))
            {
                try
                {
                    conn.Open();
                    using (var com = conn.CreateCommand())
                    {
                        com.CommandText = "SELECT * FROM " + tableName;

                        using (var r = com.ExecuteReader())
                        {
                            while (r.Read())
                            {
                                try
                                {
                                    for (int i = 0; i < r.FieldCount; i++)
                                    {
                                        Console.Write("{0} | ", r.GetValue(i).ToString());
                                    }
                                    Console.WriteLine();

                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                }
                            }
                            r.Close();
                        }
                        Console.WriteLine();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    conn.Close();
                }
            }
        }
    }
}
